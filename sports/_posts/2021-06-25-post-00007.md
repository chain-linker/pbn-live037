---
layout: post
title: "농구와 예술, 그 사이에 있는 NBA - [1. NBA 입문기]"
toc: true
---

 해외축구, 리그 오브 레전드, 국내 프로야구 율동 등 다양한 운동 경기는 최소한 하이라이트라도 몽땅 챙겨보는 나지만, 이상하게 농구 경기는 그쯤 관심이 없었다. 애초에 야구나 축구만큼 한국에서 인기가 있는 스포츠가 아니라 그런 거 같기도 하다.
 

  NBA 경기를 실례 시작한건 2017년 1월이니까 현실 얼마간 되진 않았다. 당기 그때그때 LA에서 인턴을 하던 동료 집에 동시 머문 상당히 있는데, 그때까지만 해도 NBA는커녕 농구 룰도 오직 모르던 사람이었다. 할리우드, 그리피스 천문대, 산타모니카 해변 등 이곳저곳 여행하고 친구 집에 누워있다가 불현듯이 이런 생각이 들었다.
 

  "그래도 미국까지 왔는데, 정형 유명한 NBA 한번 봐볼까?"
 

  지금이야 NBA에 무슨무슨 팀이 있고 선수들이 누가 있는지 줄줄 꿰차고 있지만, 당시에는 음밀히 유명한 LA 레이커스, 더욱이 마이클 조던의 팀이었던 시카고 불스 이렇게 두 팀밖에 몰랐다. 아무것도 모르면 정말로 구글링이지. LA에 있으니 레이커스 경기를 봐야겠다 마음을 먹고경기 스케줄도 찾아보고 티켓도 사려고 했다.
 

  "어 근데 찾아보니까 금방 대개 꼴찌네? 유명한 팀 맞아?"
 

 

  2019-2020 시즌 마무리가 한창인 지금은 새로이 엄청나게 강한 팀이 된 LA 레이커스지만, 2016-2017 시즌의 레이커스는 형편없는 팀이었다. NBA는 서부리그와 동부리그 15팀, 총 30개의 팀들이 각각 리그에서 경기를 치르는데, 레이커스는 서부리그에서 14위에 위치해 있었다. 에라 모르겠다 티켓 가격이나 한번 보자는 심정으로 검색을 다시금 시작했다.
 

  "3층 끝자리가 100달러? 어쩌면 바닥을 기는 팀인데 티켓은 어째 이렇게 비싸?"
 

  그렇다. 유명한 팀이기에 꼴찌여도 티켓값이 비쌌다. 설령 그래도 지는 경기를 100달러나 주고 보기에는 돈이 매우 아까웠다. 이렇게 NBA를 포기하긴 싫어서 다른 잘하는 팀들이 LA 근처에 있나 하는 마음으로 리그 순위표를 보던 와중에 눈에 띄는 팀 하나가 있었다. LA 클리퍼스였다.
 

 

  애초에 이런 팀이 있는지도 몰랐지만, 댁네 제때 나름 상위권이라 할 핵심 있는 4위를 달리고 있었다. 심지어 찾아보니 경기장도 레이커스와 똑같은 경기장을 사용했다. 야구로 비유해보자면 두산과 엘지가 함께 잠실야구장을 사용하는 격. 티켓 가격도 3층 상당히 앞자리인데 30달러에서 40달러 사이었던 거로 기억한다. 가격도 두 세배 저렴하고 상위권 팀인 클리퍼스 운동경기 티켓을 사는 게 훨씬 이득이지. 나란히 볼 친구를 구해 바로 두 장을 구매했다. 나중에 찾아보니 팀 창단한 손 그리도 오래되지도 않았고, 우승해본 경험도 없는 팀이라 인기가 때문에 많지 않아서 티켓 가격도 제법 저렴한 것이었다. 싼 데는 뭐든지 대다수 이유가 있다
 

  친구 차를 얻어 타고 경기장에 도착했다. 해외여행 내포 다른 나라를 여러 번 가봤지만 사회운동 경기를 보는 건 이번이 처음인지라 설레는 마음으로 티켓 검사를 하고 경기장 안으로 들어갔다. 오 그런데 신기하게도 경기장 안에 바가 있었다. 그야말로 미국인들은 술을 참것 좋아한다. 벌써 뭘 먹고 왔기 그렇게 어쨌든 보기로 하고 자리에 앉기 위해 문을 들어갔는데, 경기장의 크기와 수많은 관중들, 게다가 함성에 압도당해버렸다. 이게 단판 직관의 매력이구나. 룰도 언제나없이 모르고 심지어 작금 경기에 아무아무 선수들이 뛰는지 하나도 모르는데, 체육 개막 전부터 기대가 되는 느낌이었다.
 

  경기 소감은? 백점 만점에 백점이라는 표현은 이럴때 쓰는 거다 싶었다. 관중들의 환호성, 아울러 3층에서도 생생하게 보이는 선수들의 경이로운 움직임. 취중 선수들이 튀어올라 덩크를 할 겨를 느껴지는 전율은 이루 말할 무망지복 없을 정도였다. 이게 실지로 농구 골대를 보면 알지만, 나름 큰 키인 나조차도 덩크를 절대로 할 수가 없을 정도로 골대가 매우 높게 위치해있다. 이걸 저렇게 쉽게 쉽게 덩크를 한다고? 농구공이 몹시 무거운데 그걸 저렇게 자유자재로 드리블하고 멀리서 3점 슛을 꽂아 넣는다고? 산재 공을 저렇게 패스해서 슛을 한다고? 와 같은 감탄사를 계속해서 연발하며 봤던 기억이 난다. 그때부터 NBA를 정히 챙겨봐야겠다는 마음을 가지고 이것저것 찾아보기도 하고 농잘알 친구에게 많은걸 물어보다 보니, 이젠 대다수 밤낮 경기는 하이라이트라도 챙겨보는 수준에 이르렀다. 지금이야 역병 그렇게 난리라 얼마간 미국에 갈 아주 없겠지만, 만일[토토 커뮤니티](https://savordesk.ga/sports/post-00001.html){:rel="nofollow"} 점포 된다면 NBA 경기는 변함없이 새로이 직관하고 싶다.
 

 

 

 

